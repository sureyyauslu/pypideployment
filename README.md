# Hello World

This is an example project demonstrating how to publish a python module to PyPI

## Installation
Run following to install

```python
pip install helloworld
```

## Usage

```python
from hellloworld import say_hello

# Generate "Hello,World!"
say_hello()

# Generate "Hello, Everybody!"
say_hello("Everybody")
```

