
from setuptools import setup

with open("README.md","r") as fh:
    readme = fh.read()

setup(
    name='helloworld',
    url='https://gitlab.com/sureyyauslu/pypideployment',
    author='Solid Squad',
    author_email='',
    version='0.0.1',
    description='Say hello!',
    py_modules = ["helloworld"],
    package_dir={'':'src'},
    setup_requires=['wheel'],
    long_description=readme,
    long_desctiption_content_type = "text/markdown",

    entry_points={
        'console_scripts':['greet=helloworld:say_hello']
    }
)